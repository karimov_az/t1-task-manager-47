package ru.t1.karimov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.repository.dto.IDtoRepository;
import ru.t1.karimov.tm.dto.model.AbstractDtoModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IDtoService<M extends AbstractDtoModel> extends IDtoRepository<M> {

    @NotNull
    M add(@Nullable M model) throws Exception;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws Exception;

    @Nullable
    M findOneById(@Nullable String id) throws Exception;

    @Nullable
    M findOneByIndex(@Nullable Integer index) throws Exception;

    @NotNull
    Long getSize() throws Exception;

    void removeAll() throws Exception;

    void removeOne(@Nullable M model) throws Exception;

    void removeOneById(@Nullable String id) throws Exception;

    void removeOneByIndex(@Nullable Integer index) throws Exception;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws Exception;

    void update(@Nullable M model) throws Exception;

}
