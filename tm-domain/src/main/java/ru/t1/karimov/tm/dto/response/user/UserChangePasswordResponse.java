package ru.t1.karimov.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.dto.response.AbstractUserResponse;

@Getter
@Setter
@NoArgsConstructor
public final class UserChangePasswordResponse extends AbstractUserResponse {

    public UserChangePasswordResponse(@NotNull final UserDto user) {
        super(user);
    }

}
