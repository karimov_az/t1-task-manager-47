package ru.t1.karimov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.model.IWBS;
import ru.t1.karimov.tm.enumerated.Status;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @NotNull
    @Column(nullable = false)
    private String description = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    public Task(
            @NotNull final User user,
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Status status
    ) {
        super(user);
        this.name = name;
        this.description = description;
        this.status = status;
    }

}
